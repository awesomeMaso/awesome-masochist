# Awesome Masochist

*masochist* n, ([sb] who likes sexual suffering) (精神医学), マゾヒスト、マゾ、被虐性愛者 

> 本文档为受各个领域同好影响而建立，为方便M系同好在冲浪娱乐手冲的过程，减少时间代价及门槛，所整理的中文M系大全文档，目的包括但不限于罗列优秀作品，科学上网工具，购买外国成人产品方式，工具类软件及自用程序开发等。
>
> （看到镜像大佬开发镜像后，登录流畅方便，内容更加活跃，及许多技术大佬的贡献，准备逐步建立维护一个方便大众的面向M系资源的文档，欢迎pull request及issue）
>
> 本文档仅供交流用途，如有侵犯作者作品，联系后将立刻删除。



## 目录 Contents

[TOC]



## 入门 Getting Started

### 语言

目前互联网社区，具有大量M系资源，且适合中文人群(可以在可承受的范围内 通过学习语言或使用翻译工具阅读的)  的必备语言包括：

- 中文（各类女主天地、二次元社区、Twitter社区 均有中文M系内容）   基本技能
- 英文 （各类欧美Porn、自制Femdom内容、Twitter博主）      基本中文人群的第一门外语，达到英语四六级的较为容易，基本只需多了解一些Femdom相关专业词语，阅读和听力基本无障碍
- 日语  （各类DLsite成人作品、二次元色情漫画、音声作品、三次元成人AV）   M系必备第二外语，相较于英语社区，日语M系作品通常更注重内心及情绪氛围描写，为了更好的欣赏作品，建议N2-N3的水平是需要具备的，最低限度也建议学会五十音谱。

### 社区

>  由于社区是交换经验、讨论问题、分享资源的集中地，以下列举建议注册自己的账号的相关社区：

由于除了M系资源聚合，专注且仅有M向内容的社区较少，以下列举：

- [M系镜像 - 首页](https://mirror.chromaso.net/)
- [M系源站](http://forum.mazochina.com/)
- [女主天地](http://nztds25.com/)
- [侃胡姬论坛](https://www.kanhuji.com)

与M系内容较为相关(有部分M系资源及讨论)的社区包括：

- [南+ South Plus - powered by Pu!mdHd](https://www.south-plus.net/)      二次元论坛
- [雨中小町-听说下雨天音声和耳机更配哦 - Powered by Discuz!](https://rainkmc.com/forum.php)    同人音声论坛
- [登錄 - GiantessNight - Powered by Discuz!](https://giantessnight.com/gnforum2012/member.php?mod=logging&action=login&referer=https%3A%2F%2Fgiantessnight.com%2Fgnforum2012%2Fforum.php)    GTS论坛（巨大娘论坛  Giantess: 女巨人）
- [主页 / Twitter](https://twitter.com/home)  Twitter 中日英 福利姬  M风俗店 女王个人账号

### 官网

> 以下列举国外合法的色情作品官网，及~~国内~~中文有关作品网站~~官网~~

日语：

- [下载同人志・同人游戏・同人音声・ASMR，就在「DLsite 同人 - R18」](https://www.dlsite.com/maniax)    大量二次元同人 漫画、音声、游戏、动漫作品
- [pixiv](https://www.pixiv.net/)  二次元同人插画及小说
- https://ci-en.net/  社团资讯赞助网站
- [小说家| 検索](https://noc.syosetu.com/search/search/?word=R-18)    小说网站

中文：

- 唯爱足论坛     中文小说

英文：

- https://www.patreon.com/  英文社团作者赞助网站

### 同好自建/~~盗版~~分享集合网站

> 部分使用方便，资源数量巨大，免去自购搜索的网站，建议自用，但不建议传播，请同好自制。~~任意传播作者心血作品会减少各种作品原作者的收入，不道德且无异于涸泽而渔~~。

- https://f95zone.to/   英文论文， 游戏分享
- https://asmr.one/   日文音声作品，包含可筛选中文字幕作品
- https://www.asmrgay.com/   中文英文日文音声作品
- https://8kun.top/hypno/index.html  英文色情催眠音声作品
- https://www.javlibrary.com/cn/ 日文三次元av作品
- https://e-hentai.org/   E-hentai 不需多说 
- https://nhentai.to/  n-hentai ，e-hentai上不去时候的替代品平替

### 常用标签 Tags

> 由于在各自网站搜索及筛选作品时，M向作品较少，需要采用标签Tag的方式筛选我们所需的作品，以下例举常用于Pixiv DLsite Pornhub Twitter等网站常用的tags。

- 日语
  - 逆転無し、足コキ、男性受け、オナサポ、言葉責め、焦らし逆レイプ、SM、露出、羞恥/恥辱、首輪/鎖/拘束具、洗脳、童貞、お姉さん、ASMR、オナサポ、逆リョナ、玉潰し、金的、玉責め、女性上位、拷問、体格差、ドMホイホイ、敵女、悪の組織、女王様、長手袋、我々の業界ではご褒美です、臭いフェチ、M男、いじめ、奴隷、CFNM、SM、サキュバス、顔面騎乗、赤ちゃんプレイ、あまあま、幼児化、ロリ、メスガキ、魅了、首輪、ペニスリード、色仕掛け、負け癖、尻コキ、騎乗位、韓女様、ゴミ汁、お漏らし、貢ぎマゾ
- 英语
  - Femdom、Puppy、Training、SPH、Brainwash、footjob、 SM、 exposure、 domination、MILF、 ASMR、 torture、JOI、 enemy woman、 Queen、m man、 bullied、 slave、 cfnm、 SM、 face riding、 collar

## 平台设备 Platform

常用硬件及平台设备包括：

- Win10
- 安卓手机

> 以下列举一些为了享受M系娱乐作品，应该在平台设备上完成的初始化和修改操作：
>
> Win10：
>
> - Windows的账户名和存放M向游戏等作品，应当都为英文，避免目录中有特殊字符，导致部分程序无法运行。
> - 安装翻墙软件，购买廉价实惠的VPN机场，Clash/SSR/Vray2等
>
> 安卓：
>
> - 安装翻墙软件，购买廉价实惠的VPN机场，Clash/SSR/Vray2等
> - 安装Google Play
> - 安装部分同好分享的聚集地常用app，如Discord、Telegram

## 电影 Movies

> 包括软色情、色情作品

软色情电影作品：

- 《Venus in fur》(穿裘皮的维纳斯)
- 《解禁男女》  主演: 徐珠贤 / 李濬荣 语言: 韩语 类型: 喜剧 / 爱情
- [My Mistress (2014) - IMDb](https://www.imdb.com/title/tt2622874/?ref_=kw_li_tt)
- [Venus in Furs (1994) - IMDb](https://www.imdb.com/title/tt0111602/?ref_=kw_li_tt)

色情电影av作品：

- https://missav.com/cn/abp-984

## 游戏 Games

> M向游戏主要为个体或小社团开发的独立游戏，通常采用RPG Maker VX MV系列，视觉小说galgame，Unity，Unreal等格式开发，通常需要在Windows上运行，日文游戏常需要配合使用转区软件，否则出现大量乱码。

帖子：

- [M系镜像 - M系游戏作品 测评贴](https://mirror.chromaso.net/thread/48776)

RPG类游戏：

- Tower of trample
- Katalist
- Sadiubus_v1.0.1

视觉小说类游戏：

- Msize社团所有作品
  - http://msize.sakura.ne.jp/
  - 悪女の栄冠
  - 育精
- Excess M社团
- 女系家族の犬になった仆 社团名くりまんじゅう
- エルミア剣闘物语-少年剣士VS戦う乙女たち-

回合制游戏：

- boko877全部作品
  - http://boko.main.jp/boko-game.html
- Under the Witch
  - https://twitter.com/numericgazer?lang=ja

## 音声 Voice Work

> 建议使用https://asmr.one、及https://www.asmrgay.com/   网站作为入门

优秀经典日文作品：

- [RJ332327 やわらか罵倒で遊んであげます♪～お兄さんのとっても可愛いんですね♪～ - ASMR Online](https://asmr.one/work/RJ332327)
  - 骂倒类、短小羞辱类 社团
- [RJ378229 高貴なエルフ様による劣等種族洗脳教育 - ASMR Online](https://asmr.one/work/RJ378229)
  - 互动类、异世界幻想类 M向社团
- [RJ295351 月収100万円美脚女子VS底辺童貞貢ぎマゾ - ASMR Online](https://asmr.one/work/RJ295351)
  - 贡系 社团 天花板！ 	逢坂成美+贡系！ 谁懂！
- [RJ238914 リディクル・エクスタシー催眠～女の子たちに嘲笑される快感～ - ASMR Online](https://asmr.one/work/RJ238914)
  - FAS  著名M向催眠社团，诸多经典之作，量大管饱
- [RJ167627 極悪メイドの最凶搾精遊戯I - ASMR Online](https://asmr.one/work/RJ167627)
  - 重度 M向自慰支援社团モルモットストリップ ，已不再活跃  
- [RJ154105 男を思い通りに操る術、教えます。 第1章 - ASMR Online](https://asmr.one/work/RJ154105)
  - 经典 射精管理 社团， 配合文章食用
  - https://chastityfun.blog.fc2.com/   射精管理日文小说
- [RJ213550 吸精サキュバスの幼児化調教催眠3 - ASMR Online](https://asmr.one/work/RJ213550)
  - ホワイトピンク ！  暖甜风格的M系催眠，系列作品
- [RJ121353 おゆうぎのじかん - ASMR Online](https://asmr.one/work/RJ121353)
  - Candle Man ！最老牌的M向催眠社团，上古大神社团，催眠效果超好
- [RJ179557 让你变成废柴的叭噗叭噗Play 治愈的返婴训练 - ASMR Online](https://asmr.one/work/RJ179557)
  - 母系作品，ABDL，婴儿退行的 典型社团
- [RJ223075 バブみ射精管理～聖母のような微笑みにバブみを感じながら、ときにやさしく、ときに厳しく、何度も何度も射精管理されたい～ - ASMR Online](https://asmr.one/work/RJ223075)
  - 重度自慰支援 M向社团  类似モルモットストリップ继承人
- [RJ227351 音声で手コキ 射精はゲームオーバー★快楽我慢ゲーム「早漏遺伝子滅亡計画2」〜優しい女の子からの攻撃に耐え、射精を我慢せよ〜 - ASMR Online](https://asmr.one/work/RJ227351)
  - 072lab 专注自慰支援100年，流程 略微程序化僵硬化
- [RJ01034675 【日配中字】LV99→LV0 被公主殿下夺走一切的勇者 - ASMR Online](https://asmr.one/work/RJ01034675)
  - CreamPan，进击吧！奶油面包佬！！为了M男の荣耀！
- [RJ249799 家畜射精 お姉さんの指示でイク工場見学会 - ASMR Online](https://asmr.one/work/RJ249799)
  - 较为优秀的自慰支援论坛，比072lab更多花样，值得072lab学习！！

中文字幕日文作品：

- [Search by tag:男性受 - ASMR Online](https://asmr.one/works?keyword=%24tag%3A%E7%94%B7%E6%80%A7%E5%8F%97%24)   集合 建议自行搜寻感兴趣的

中文音声作品：

- [中文音声 | ASMR基佬中心](https://www.asmrgay.com/asmr/%E4%B8%AD%E6%96%87%E9%9F%B3%E5%A3%B0)
- 推荐社团： 林三岁、[七重极乐](https://www.asmrgay.com/asmr/中文音声/七重极乐)、[南征](https://www.asmrgay.com/asmr/中文音声/南征)、[小野猫](https://www.asmrgay.com/asmr/中文音声/小野猫)、[无名音声社](https://www.asmrgay.com/asmr/中文音声/无名音声社)、[梧桐别苑](https://www.asmrgay.com/asmr/中文音声/梧桐别苑)、[瑶酱萌萌哒](https://www.asmrgay.com/asmr/中文音声/瑶酱萌萌哒)、[糖果屋](https://www.asmrgay.com/asmr/中文音声/糖果屋)、[逍遥音声](https://www.asmrgay.com/asmr/中文音声/逍遥音声)、[阿木木](https://www.asmrgay.com/asmr/中文音声/阿木木)、[紫眸](https://www.asmrgay.com/asmr/中文音声/紫眸)、[步非烟](https://www.asmrgay.com/asmr/中文音声/步非烟)
- `没错！步非烟！就是放在最后一个！！！流水线制造的粗口才不色！不是地图炮！！国内自认为是在搞SM的家伙们！！！认真思考一下SM的本质吧！！不是骂TMD和CNM就是SM！！SM才不是这样的东西！CNM！迎接M男的愤怒吧！`
- [晓春幻想乐园](https://www.kanhuji.com/panel/xiaochun/) - 改编和翻译部分国外催眠作品同时也融合了自己的创作，花钱请了CV录制高质量催眠音频，目前在侃胡姬论坛出售所有作品，包括原创小说

英文催眠作品：

- Nikki fatale  （推荐入门用，`年轻人的第一部Erotic Hypnosis！`）

  - [Internet Archive: View Archive  下载](https://ia803006.us.archive.org/view_archive.php?archive=/10/items/nikki.fatale/Nikki.Fatale.zip)
  - [(3) Nikki Fatale: An Ongoing Review : EroticHypnosis 评论感想](https://www.reddit.com/r/EroticHypnosis/comments/o8ioud/nikki_fatale_an_ongoing_review/)

- Mistress Carol （推荐入门用，`年轻人的第二部Erotic Hypnosis！`）

  - [Erotic Hypnosis by Mistress Carol](https://myhypnoticdomain.com/)

- Mistress Amethyst

- Fiona Clearwater 
  Fiona Clearwater works best for me. Normal voice tones (her D/s audios have a more forceful tone), and a good variety of audios. She shares a Youtube channel with Ultrahypnosis....her playlist link: 
  https://www.youtube.com/playlist?list=PLIPnyalgdSAXxpnHzTBQ_F2K7x3dHzvyl
  I would recommend starting with Fiona's Hypnotic Induction first, to establish her basic sleep triggers.

- Lady Aurora

  https://soundgasm.net/u/The_Lady_Aurora/
  https://theladyaurora.com/store-2/Free-c72601447

- 帖子：

  https://www.reddit.com/r/EroticHypnosis/comments/qcwalo/please_recommend_some_vanilla_erotic_hypnosis/
  https://www.reddit.com/r/EroticHypnosis/comments/o8ioud/nikki_fatale_an_ongoing_review/
  https://www.reddit.com/r/EdgingTalk/comments/pynprw/owned_by_hypno_m/
  https://www.reddit.com/r/EroticHypnosis/comments/azkh63/male_subject_needing_more_because/
  https://www.reddit.com/r/EroticHypnosis/comments/9taa5a/your_favourite_all_time_hypnotists/

## 小说 Novels

> 列举小说，排名不分先后，不评分，仅以类型和语言进行划分

### 中文：

- 幻想类：
  - [M系镜像 - 【轻H剧情向/新人新书】魅魔学院的反逆者](https://mirror.chromaso.net/thread/1073742791)
  - [M系镜像 - 私立魅魔樱花瓣学院(榨精，女上男下，足交，丸吞，魅魔......）](https://mirror.chromaso.net/thread/1073743417)
  - [M系镜像 - 帝国王子的复仇之旅（长篇）](https://mirror.chromaso.net/thread/1073743005)
  - [M系镜像 - 【陈明的穿越之旅】在被女性支配的世界中生存下去（超能女性、女怪人、魔物娘、亚人）](https://mirror.chromaso.net/thread/50053)
  - [M系镜像 - 长篇《异世界修罗场生存指南》（更新至第二十章）](https://mirror.chromaso.net/thread/54645)
- M格斗类：
  - [M系镜像 - 【脑机格斗】](https://mirror.chromaso.net/thread/1073743973)
  - [M系镜像 - 【M格斗】【连载中】天鹅湖](https://mirror.chromaso.net/thread/1073744509)
  - [M系镜像 - 【M 格斗】都市阿玛宗 ](https://mirror.chromaso.net/thread/54189)
- 精神洗脑类：
  - [M系镜像 - 【原创】《年级第二是助眠区up主》（精神控制、催眠、洗脑、自慰支援、色诱）](https://mirror.chromaso.net/thread/54465)
  - [M系镜像 - 【新人原创】在敌对国包养小三是外交官的专属福利！（1月31日第17页更新至【12】【败露（上）】）](https://mirror.chromaso.net/thread/32039)

- BF 性战类：
  - [M系镜像 - 阴阳采战录【续写】](https://mirror.chromaso.net/thread/49362)
- 女怪人类：
  - [「【绯红】【冷饭重置】《葫芦在劫难逃》不定期更新」/「阿加雷斯的蛛网」](https://www.pixiv.net/novel/series/7606905)
  - 
- 真实三次元类：
  - [M系镜像 - 偏偏要做你的 M〖原创连载/暂时断更〗](https://mirror.chromaso.net/thread/29311)
  - [M系镜像 - 【原创】（更新2021.3.11）望晴廿年事](https://mirror.chromaso.net/thread/31897)
  - [M系镜像 - 《她》（春节篇现已更新，春节快乐）](https://mirror.chromaso.net/thread/49782)
  - [M系镜像 - Re: 搬运工-----大全女S佳作《禁欲天堂》第一册（妻绝）完结撒花！第二册（妻变）-禁之渊 开番 不定时更新](https://mirror.chromaso.net/thread/36223)
  - [M系镜像 - 当M男是一条幸福的不归路](https://mirror.chromaso.net/thread/8693)

### 日文：

- サキュバスとの闘い    pixiv格斗魅魔文
- 高級M性感 在籍艦プロフィール一覧  舰娘m风俗
- 三匹のドM  校园m文
- [「【連載官能小説】清楚で優しい妻の奴隷になる方法について」/「慶多胤](https://www.pixiv.net/novel/series/1316836)
- [「シャドバ２次」/「妖術師M」のシリーズ \[pixiv\]](https://www.pixiv.net/novel/series/778196)
- 

### 英文：

- ...



## 图片 Pictures

- Empty

## 工具 Tools

> 列举常用工具，包括游戏音声翻译等用途



翻译工具：

- [百度翻译-200种语言互译、沟通全世界！](https://fanyi.baidu.com/)   百度翻译在线



游戏工具：

- Locale.Emulator.2.4.1.0  日文转区工具
- Visual Novel Reader V3   日文视觉小说游戏翻译
- RPGVXAce  RPG Maker开发工具
- CheatEngine74  修改外挂工具
- MTool   翻译工具



创作工具：

- [M系镜像 - MistressGPT：基于GPT3.5的女王虚拟调教室](https://mirror.chromaso.net/thread/55052)
- ...



## 跟随账号 Follows

> 例举 平常刷网站时候，动态较为活跃，图文质量较高的账号。

Pixiv：

- 主要包括m系涩图作者、AI图文作者
- ...

Twitter：

- 主要包括日系风俗店娘、福利姬、涩图作者
- ...

## 后续 TODO

- [ ] 添加 各个网站建议follow的账号 （pixiv、twitter、ci-en）
- [ ] 添加完善 工具类的网站及教程
- [ ] 添加 科学上网 教程 及机场推荐
- [ ] 添加完善  经典游戏
- [ ] 添加常用 油猴脚本 pixiv下载脚本

## 贡献 Contributing

项目放在gitlab，欢迎pull request！
https://gitlab.com/awesomeMaso/awesome-masochist

Your contributions are always welcome!